angular.module('testapp', [
	'ngRoute','ngMessages'
])
.config(function($routeProvider){
$routeProvider
  .when('/', {
  	templateUrl: 'views/home.html',
  	controller: 'homeCtrl'
  })
  .when('/register', {
    templateUrl: 'views/register.html',
    controller: 'registerCtrl'
  })
  .otherwise({
    redirectTo: '/'
  });
})
.controller('homeCtrl', function($log){
  $log.debug('Welcome to the testapp!');
})
.controller('registerCtrl', function($scope, $log){
	$scope.submit = function(user){
		$log.debug('submit: user = ', user);
	};
})
.directive('compareTo', compareToDirective)
.directive('validateSsn', validateSSNDirective);

function compareToDirective () {
	return {
		require: "ngModel",
		scope: {
			originalValue: "=compareTo"
		},
		link: function(scope, element, attributes, ngModel) {
			ngModel.$validators.compareTo = function(modelValue) {
				return modelValue == scope.originalValue;
			};
			scope.$watch('originalValue', function() {
				ngModel.$validate();
			});
		}
	};
}


function validateSSNDirective () {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, ele, attrs, ctrl){
			// add a parser that will process each time the value is
			// parsed into the model when the user updates it.
			ctrl.$parsers.unshift(function(value) {
				var valid;
				if(value){
					// test and set the validity after update.
					valid = isValidSSN(value);
					ctrl.$setValidity('invalidSSN', valid);
				}

				// if it's valid, return the value to the model, otherwise return undefined.
				return valid ? value : undefined;
			});

		}
	}
}


//luhn algorithm from https://gist.github.com/ShirtlessKirk/2134376
function luhnCheck (input) {
	var i,
		sum = 0,
		numdigits = input.length,
		parity = numdigits % 2,
		digit;

	// luhn algorithm
	for (i = 0; i < numdigits; i++) {
		digit = parseInt(input.charAt(i), 10);
		if (i % 2 === parity) { digit *= 2; }
		if (digit > 9) { digit -= 9; }
		sum += digit;
	}

	return (sum % 10) === 0;
}

function isValidSSN (input) {
	if (!input) {
		return false;
	}

	if (input.indexOf('-') === -1) {
		if (input.length === 10) {
			input = input.slice(0, 6) + '-' + input.slice(6);
		} else {
			input = input.slice(0, 8) + '-' + input.slice(8);
		}
	}

	if (!input.match(/^(\d{2})(\d{2})(\d{2})\-(\d{4})|(\d{4})(\d{2})(\d{2})\-(\d{4})$/)) {
		return false;
	}

	// Clean input
	input = input.replace('-', '');
	if (input.length === 12) {
		input = input.substring(2);
	}

	// Declare variables
	var year = RegExp.$1 || RegExp.$5,
		month = RegExp.$2 || RegExp.$6,
		day = RegExp.$3 || RegExp.$7;

	//unless the world suddenly starts spinning and orbiting to another groove...
	if (month > 12 || day > 31) {
		return false;
	}

	var d = new Date(year, month - 1, day);

	// Check valid date
	if (Object.prototype.toString.call(d) !== "[object Date]" || isNaN(d.getTime())) {
		return false;
	}

	return luhnCheck(input);
}
